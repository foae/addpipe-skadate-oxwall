# AddPipe Video Profiles plugin for Oxwall #
The Video Profiles plugin will enable your users to record a video presentation of themselves and display it in their profile, where other registered members can watch it. It is an addition to the usual user info and profile pictures.

# Installation instructions #
Here are the necessary steps to have take to have a fully working Video Profiles plugin in less than a minute:

### 1. Getting the plugin in your website ###
1. Make sure you have downloaded the full plugin archive `addpipe-full.zip` from [here (download link)](https://bitbucket.org/lucian-alexandru/addpipe-skadate-oxwall/downloads/addpipe-full.zip)
2. Unzip the archive. A new folder will be visible, `addpipe-full`
3. In your FTP client, go to your root folder where Oxwall is installed
4. You have to upload everything from *inside* the `addpipe-full` folder, namely `ow_plugins` and `ow_system_plugins` folders over your Oxwall installation. No files will be overwritten!
5. Go the the admin area and click **Install**.

### 2. Connecting the plugin to AddPipe.com: ###
1. Login into your AddPipe account at https://addpipe.com
2. Navigate to the **Webhook** tab and enter the web address (URL) where your Oxwall installation is located -`http://your-site.com/` or `http://your-site.com/subfolder`
4. Navigate to the **Account settings** tab and copy your **account hash** (sensitive information! Be careful with it!) and go back into your OxWall site; navigate to **AddPipe Video Profiles** plugin and finish the setup by entering your above **account hash**. 
That's it!

For any questions, use **contact@nusofthq.com**  
For support, please use **support@nusofthq.com**