<?php

/**
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is
 * licensed under The BSD license.

 * ---
 * Copyright (c) 2011, Oxwall Foundation
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice, this list of conditions and
 *  the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the Oxwall Foundation nor the names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

OW::getConfig()->addConfig('addpipe', 'setting_vars', '{}');
OW::getPluginManager()->addPluginSettingsRouteName('addpipe', 'addpipe_admin');
OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('addpipe')->getRootDir() . 'language_en.zip', 'addpipe');

/*
 * The CustomVideo Widget [User's Video Profile]
 */
$widgetService = BOL_ComponentAdminService::getInstance();
$widget = $widgetService->addWidget('BASE_CMP_CustomVideo', true); // Register the widget. The first parameter is the name of the widget class. The second parameter stands for whether the widget can be cloned, or may have only one sample on the page\\
$widgetPlace = $widgetService->addWidgetToPlace($widget, BOL_ComponentService::PLACE_PROFILE); // Add the widget to the page
$widgetService->addWidgetToPosition($widgetPlace, BOL_ComponentService::SECTION_TOP, 0); // Choose a special position on the page to add the widget. the third parameter of this function is the widget's position in a certain section. If you want to add the widget to the very bottom, you should set -1.

/*
 * Custome table for video tracking & display per userID
 */
$query = "CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "addpipe_records` (
    `internal_id` INT(6) NOT NULL AUTO_INCREMENT,
    `member_id` INT(6),
    `video_id` INT(9),
    `video_url` VARCHAR(64),
    `date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    `json` varchar(3000),
    `active` SMALLINT(1) DEFAULT 0,
    PRIMARY KEY (`internal_id`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

OW::getDbo()->query($query);