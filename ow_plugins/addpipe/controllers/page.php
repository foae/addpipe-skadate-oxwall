<?php

/**
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is
 * licensed under The BSD license.

 * ---
 * Copyright (c) 2011, Oxwall Foundation
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice, this list of conditions and
 *  the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the Oxwall Foundation nor the names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @author Lucian Alexandru <lucian@nusofthq.com>
 * @package ow_plugins.addpipe
 * @since 1.0
 */

class ADDPIPE_CTRL_Page extends OW_ActionController {

    public function __construct() {
        parent::__construct();
	}

    public function getViewerID() {

    }
    
	public function index() {

        if(!OW::getUser()->isAuthenticated())  {
            throw new AuthenticateException();
        }

        /*
         * Get a list of recorded videos for current user
         */
        $currentUserID = OW::getUser()->getId();
        $sql = "SELECT * FROM `" . OW_DB_PREFIX . "addpipe_records` WHERE member_id = {$currentUserID} AND active = '1' LIMIT 1";
        $grabLastVideo = OW::getDbo()->queryForRow($sql);

        if (!empty($grabLastVideo)) {
            $hasVideo = "document.getElementById('main-wrap').innerText += 'Your video profile is available on your profile page. Recording a new one will replace the existing video profile.';";
        } else {
            $hasVideo = "document.getElementById('main-wrap').innerText += 'You don\'t have a video profile yet, record one below. Your video profile will show up on your profile page.';";
        }

        // Add some space between the upper explanation and the video frame
        $hasVideo .= "document.getElementById('main-wrap').innerHTML += '<br />';";
        
        $params = array('siteURL' => OW_URL_HOME);
        $params['username'] = BOL_UserService::getInstance()->getDisplayName(OW::getUser()->getId());
        $params['userID'] = OW::getUser()->getId();

        $settingsArray = (array)json_decode(OW::getConfig()->getValue('addpipe', 'setting_vars'));
        $params['accountHash'] = (empty($settingsArray['accountHash'])) ? 'false' : $settingsArray['accountHash'];
        $params['videoDuration'] = (empty($settingsArray['videoDuration'])) ? 20 : $settingsArray['videoDuration'];
        $params['videoQuality'] = (empty($settingsArray['videoQuality'])) ? '480p' : $settingsArray['videoQuality'];

        if ($params['videoQuality'] == '240p') {
            $videoWidth = '320';
            $videoHeight = '270';
        } elseif ($params['videoQuality'] == '480p') {
            $videoWidth = '640';
            $videoHeight = '510';
        } else {
            $videoWidth = '640';
            $videoHeight = '390';
        }

        /* Assign values for output */
        $this->assign('addPipeParams', json_encode($params));
        $this->assign('accountHash', $params['accountHash']);
        $this->assign('userID', $params['userID']);
        $this->assign('username', $params['username']);
        $this->assign('videoDuration', $params['videoDuration']);
        $this->assign('videoQuality', $params['videoQuality']);
        $this->assign('videoWidth', $videoWidth);
        $this->assign('videoHeight', $videoHeight);
        $this->assign('hasVideo', $hasVideo);

        $this->setPageHeading(OW::getLanguage()->text('addpipe', 'index_heading'));
        $this->setPageTitle(OW::getLanguage()->text('addpipe', 'index_title'));
        $this->setPageHeadingIconClass('ow_ic_chat');        
	}
}