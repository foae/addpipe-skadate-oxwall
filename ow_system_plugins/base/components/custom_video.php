<?php

/**
 * EXHIBIT A. Common Public Attribution License Version 1.0
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the “License”);
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.oxwall.org/license. The License is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be consistent
 * with Exhibit B. Software distributed under the License is distributed on an “AS IS” basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for the specific language
 * governing rights and limitations under the License. The Original Code is Oxwall software.
 * The Initial Developer of the Original Code is Oxwall Foundation (http://www.oxwall.org/foundation).
 * All portions of the code written by Oxwall Foundation are Copyright (c) 2011. All Rights Reserved.

 * EXHIBIT B. Attribution Information
 * Attribution Copyright Notice: Copyright 2011 Oxwall Foundation. All rights reserved.
 * Attribution Phrase (not exceeding 10 words): Powered by Oxwall community software
 * Attribution URL: http://www.oxwall.org/
 * Graphic Image as provided in the Covered Code.
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a work
 * which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 */

/**
 * Custom HTML widget
 *
 * @author Lucian Alexandru <lucian@nusofthq.com>
 * @package ow_plugins.addpipe
 * @since 1.0
 */

class BASE_CMP_CustomVideo extends BASE_CLASS_Widget {

    private $content = false;

    public function __construct(BASE_CLASS_WidgetParameter $paramObject) {
        parent::__construct();

        $params = $paramObject->customParamList;

        /**/
        $extraInfo = array(
            /* Visited member's ID */
            'memberID' => $paramObject->additionalParamList['entityId'],
            'memberName' => BOL_UserService::getInstance()->getDisplayName($paramObject->additionalParamList['entityId']),
            /* Registered member's ID */
            'visitorID' => OW::getUser()->getId(),
            'visitorName' => BOL_UserService::getInstance()->getDisplayName(OW::getUser()->getId())
        );

        $sql = "SELECT `json` FROM `" . OW_DB_PREFIX . "addpipe_records` WHERE member_id = {$extraInfo['memberID']} AND active = '1' LIMIT 1";
        $grabLastVideo = OW::getDbo()->queryForRow($sql);

        if (!empty($grabLastVideo)) {
            $json = json_decode($grabLastVideo['json']);
            $json = $json->data;
            $this->content = "
                        <style type='text/css'>
                            video {
                                width: 100%;
                                height: auto;
                                text-align: center;
                                align-content: center;
                                align-self: center;
                            }
                        </style>
                        <video controls>
                            <source src='{$json->url}' type='video/mp4'>
                            Your browser does not support the video tag. Please upgrade or use a modern browser, like Google Chrome of Mozilla Firefox.
                        </video>
                        <div style='text-align: left; padding-top: 10px; font-size: 9px;'>
                            <a href='https://addpipe.com/?utm_source=oxwall-skadate-plugin&utm_medium=powered-by-link&utm_content=powered-by-link&utm_campaign=addpipe-integration' target='_blank'>Video recording powered by AddPipe.com</a>
                        </div>
                        ";
        } else {
            $this->content = "<div style='text-align: center;'>This user hasn't recorded any video profile yet.</div>";
        }

        if (!empty($params['content'])) {
            $this->content = $params['content'];
        }

    }

    public static function getSettingList() {

        $settingList = array();
        $settingList['content'] = array(
            'presentation' => self::PRESENTATION_TEXTAREA,
            'label' => OW::getLanguage()->text('addpipe', 'custom_video_content_label'),
            'value' => ''
        );

        $settingList['nl_to_br'] = array(
            'presentation' => self::PRESENTATION_CHECKBOX,
            'label' => OW::getLanguage()->text('addpipe', 'custom_video_nl2br_label'),
            'value' => '0'
        );

        return $settingList;
    }

    public static function processSettingList($settings, $place, $isAdmin) {
        return parent::processSettingList($settings, $place, $isAdmin);
    }

    public static function getStandardSettingValueList() {
        return array(
            self::SETTING_TITLE => OW::getLanguage()->text('addpipe', 'custom_video_default_title')
        );
    }

    public static function getAccess() {
        return self::ACCESS_MEMBER;
    }

    public function onBeforeRender() {
        $this->assign('content', $this->content);
    }
}