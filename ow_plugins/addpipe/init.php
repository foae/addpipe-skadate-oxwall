<?php

/**
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is
 * licensed under The BSD license.

 * ---
 * Copyright (c) 2011, Oxwall Foundation
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice, this list of conditions and
 *  the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the Oxwall Foundation nor the names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

OW::getRouter()->addRoute(new OW_Route('addpipe_admin', 'admin/plugins/addpipe', 'ADDPIPE_CTRL_Admin', 'index'));
OW::getRouter()->addRoute(new OW_Route('addpipe_main', 'addpipe', 'ADDPIPE_CTRL_Page', 'index'));

function addpipe_ads_enabled (BASE_CLASS_EventCollector $event) {
    $event->add('addpipe');
}

/*
 * AddPipe webhook implementation
 */
if (isset($_POST['payload']) && !empty($_POST['payload'])) {

    /* Process the request */
    $json = (isset($_POST['payload'])) ? $_POST['payload'] : die('Missing payload.');
    $json = str_replace('\r', '', $json);
    $json = str_replace('\n', '', $json);
    $json = str_replace("\\", '', $json);
    $json = json_decode($json);

    /* Slice the custom values from the payload */
    $payload = explode(',', $json->data->payload);
    $accountHash = (string) $payload[0];
    $memberID = (int) $payload[1];

    /* Build the Update query */
    $queryUpdate = "
                    UPDATE `" . OW_DB_PREFIX . "addpipe_records`
                    SET `active` = 0
                    WHERE `member_id` = '{$memberID}';
                    ";

    /* Build the Insert query */
    $jsonData = json_encode($json); // Encode the Object back to json
    $queryInsert = "
                    INSERT INTO `" . OW_DB_PREFIX . "addpipe_records` (`member_id`, `video_id`, `video_url`, `json`, `active`)
                    VALUES ('{$memberID}', '{$json->data->id}', '{$json->data->snapshotUrl}', '{$jsonData}', 1);
                    ";

    /* Grab the accountHash from the DB */
    $settingsArray = (array) json_decode(OW::getConfig()->getValue('addpipe', 'setting_vars'));

    /* Check the contents and the account hash: TBD */
    if ($settingsArray['accountHash'] == $accountHash) {

        // insert into db ; give something back and die
        @OW::getDbo()->query($queryUpdate); // Update first, insert later to avoid updating the last recorded video
        @OW::getDbo()->query($queryInsert);
        http_response_code(200);
        $_POST['payload'] .= "------------- 200 OK! -------------" . PHP_EOL;
        file_put_contents(dirname(__FILE__) . '/_addpipe_payloads.log', print_r($_POST['payload'], true) . PHP_EOL, LOCK_EX | FILE_APPEND);
        die('200 - OK.');

    } else {

        // The Payload isn't correct
        http_response_code(417);
        $_POST['payload'] .= "------------- Error 417 -------------" . PHP_EOL;
        file_put_contents(dirname(__FILE__) . '/_addpipe_errors.log', print_r($_POST['payload'], true) . PHP_EOL, LOCK_EX | FILE_APPEND);
        die('417 - Not OK.');

    }

    // Default: $_POST['payload'] exists, but could not be proccessed
    $_POST['payload'] .= "------------- Unknown error -------------" . PHP_EOL;
    file_put_contents(dirname(__FILE__) . '/_addpipe_errors.log', print_r($_POST['payload'], true) . PHP_EOL, LOCK_EX | FILE_APPEND);

}